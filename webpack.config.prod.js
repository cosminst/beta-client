const webpack = require('webpack');

module.exports = {
    mode: 'production',
    plugins: [
        new webpack.DefinePlugin({
            apiHost: '"https://api.cosmin.space"'
        })
    ]
};