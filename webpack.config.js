const config = require('./webpack.config.common');
let environmentConfig = null;

switch (process.env.NODE_ENV) {
    case 'production':
        environmentConfig = require('./webpack.config.prod');
        break;

    case 'development':
    default:
        environmentConfig = require('./webpack.config.dev');
        break;
}

config.mode = environmentConfig.mode;
config.devtool = environmentConfig.devtool;
config.plugins = config.plugins.concat(environmentConfig.plugins);
config.devServer = environmentConfig.devServer;

module.exports = config;