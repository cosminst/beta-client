const webpack = require('webpack');
const path = require("path");

module.exports = {
    mode: 'development',
    // devtool: 'inline-source-map',
    plugins: [
        new webpack.DefinePlugin({
            apiHost: '"http://api.cosmin.local:8888"'
        })
    ],
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'),
        compress: false,
        port: 8080,
        historyApiFallback: true,
        allowedHosts: [
            'cosmin.local'
        ]
    }
};