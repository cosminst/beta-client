import i18next from 'i18next';
import xhr from 'i18next-xhr-backend';
import namespaces from '../../locale';
import {setLocale} from 'react-redux-i18n';
import storageWrapper from '../utils/storageWrapper';

export default () => ({
    dispatch: () => {},

    /**
     * Sets the language for the application
     * @param {string} language - the language ISO code
     */
    changeLanguage(language) {
        return new Promise (resolve => {
            i18next.use(xhr).init({
                fallbackLng: 'en',
                lng: language,
                ns: namespaces(),
                defaultNS: 'general',
                debug: false,
                backend: {
                    loadPath: '/locale/{{lng}}/{{ns}}.json'
                }
            }, () => {
                this.dispatch(setLocale(language));
                storageWrapper().set('app-settings', {language});
                resolve();
            });
        });
    }
});