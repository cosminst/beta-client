import {push, goBack} from 'connected-react-router';
import routes from '../config/routes';

export default () => ({
    /**
     * Navigate to a certain route
     * @param {string} route - the route to navigate to
     * @param {Object} params - route parameters
     * @return {Promise}
     */
    navigate(route, params) {
        const path = routes(route);
        this.dispatch(push(path, params));
        return Promise.resolve();
    },

    /**
     * Gets the computed URL of a certain route
     * @param {string} route - the route to compute
     * @param {Object} params - the parameters to use
     * @return {Promise<string>}
     */
    getComputedURL(route, params = {}) {
        let computedRoute = routes(route);
        Object.keys(params || {}).forEach(key => {
            computedRoute = computedRoute.replace(`:${key}`, params[key]);
        });

        return computedRoute;
    },

    goBack() {
        this.dispatch(goBack());
        return Promise.resolve();
    }
});