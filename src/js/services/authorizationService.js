import {promptLogin} from "../reducers/user";

export default () => ({
    dispatch: () => {},

    /**
     * Prompts the login dialog
     * @param {string} message - the messages to prompt in the login prompt
     * @return {Promise}
     */
    promptLogin(message) {
        this.dispatch(promptLogin(message));
        return Promise.resolve();
    }
});