import navigationService from './navigationService';
import localizationService from './localizationService';
import authorizationService from "./authorizationService";

let dispatch = null;

const SERVICES = {
    navigation: navigationService,
    localization: localizationService,
    authorization: authorizationService,
};

export default (...params) => ({
    /**
     * Retrieve a service by its key
     * @param {string} serviceKey - the key of the required service
     * @return {Object}
     */
    get(serviceKey) {
        if (SERVICES.hasOwnProperty(serviceKey)) {
            const service = SERVICES[serviceKey](...params);
            service.dispatch = dispatch;
            return service;
        } else {
            throw new Error(`Service "${serviceKey}" does not exist.`);
        }
    },

    /**
     * Sets the dispatcher that's supposed to be injected in every service
     * @param {function} dispatcher - dispatch function
     * @return {this}
     */
    setDispatcher(dispatcher) {
        dispatch = dispatcher;
        return this;
    },

    /**
     * Returns the dispatcher
     * @return {*}
     */
    getDispatcher() {
        return dispatch;
    }
});