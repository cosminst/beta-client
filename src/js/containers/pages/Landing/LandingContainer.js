import {connect} from 'react-redux';
import Landing from '../../../components/pages/Landing/Landing';
import Memberships from '../../../collections/Memberships';
import Membership from '../../../models/Membership';
import User from "../../../models/User";

const mapStateToProps = state => ({
    user: new User(state.authorization.user),
    loggedIn: state.authorization.loggedIn
});

const mapActionsToProps = dispatch => ({
    /**
     * Returns a collection containing the available membership types
     * @return {Promise<Memberships>}
     */
    getMembershipTypes() {
        return new Memberships().filter();
    },

    /**
     *
     * Submits the registration form
     * @param {Object} value - the form value
     * @return {Promise}
     */
    submitRegistration(value) {
        const membership = new Membership(value);
        return membership.save();
    }
});

export default connect(mapStateToProps, mapActionsToProps)(Landing);