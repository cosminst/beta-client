import {connect} from 'react-redux';
import Gateway from '../../components/pages/Gateway';
import gateway from '../../config/gateway';
import User from '../../models/User';
import {hidePrompt} from "../../reducers/user";
import di from '../../services/di';
import storageWrapper from '../../utils/storageWrapper';

const mapStateToProps = state => ({
    i18n: state.i18n,
    user: new User(state.authorization.user),
    showPrompt: state.authorization.prompt,
    promptMessage: state.authorization.promptMessage,
});

const mapActionsToProps = dispatch => ({
    /**
     * Return the gateway configuration
     * @return {Object}
     */
    getConfiguration() {
        return gateway();
    },

    /**
     * Hide the login prompt
     * @return {Object}
     */
    hidePrompt() {
        dispatch(hidePrompt());
    },

    /**
     * Logs the user out
     * @return {Promise}
     */
    logout() {
        storageWrapper().delete('user');
        return di().get('navigation').navigate('login');
    }
});

export default connect(mapStateToProps, mapActionsToProps)(Gateway);