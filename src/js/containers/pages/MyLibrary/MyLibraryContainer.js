import {connect} from 'react-redux';
import MyLibrary from '../../../components/pages/MyLibrary/MyLibrary';
import di from '../../../services/di';
import ajax from '../../../utils/ajax';

const mapActionsToProps = dispatch => ({
    navigation: di().get('navigation'),

    import(data) {
        return ajax('/books/import')
            .authorize()
            .post({data});
    },

    goBack() {
        return this.navigation.goBack();
    },

    goToImport() {
        return this.navigation.navigate('import')
    }
});

export default connect(null, mapActionsToProps)(MyLibrary);