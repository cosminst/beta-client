import {connect} from 'react-redux';
import BookList from '../../../components/pages/Books/BookPage';
import di from '../../../services/di';
import Book from '../../../models/Book';

const mapStateToProps = state =>  ({});

const mapActionsToProps = dispatch => ({

    /**
     * Checks if the URL has the book isbn
     * If it does, the isbn is used to open the book modal
     * @return {boolean}
     */
    isBookPage() {
        return typeof this.match.params.isbn !== 'undefined';
    },

    /**
     * Gets the isbn from the url
     * @return {Book}
     */
    getUrlBook() {
        return new Book()
            .setIdentifier(this.match.params.isbn);
    },

    /**
     * Closes the modal
     * @return {Promise}
     */
    hideModal() {
        return di().get('navigation')
            .navigate('books', {isbn: null});
    }
});

export default connect(mapStateToProps, mapActionsToProps)(BookList);