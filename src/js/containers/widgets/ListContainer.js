import {connect} from "react-redux";
import List from "../../components/widgets/list/List";

const mapStateToProps = state => ({});
const mapActionsToProps = dispatch => ({

    /**
     * Fetches a list of models
     * @return {Promise<Collection>}
     */
    fetchList(filters) {
        const collection = new this.collection();
        return collection.filter(filters);
    }
});

export default connect(mapStateToProps, mapActionsToProps)(List);