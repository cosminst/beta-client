import {connect} from 'react-redux';
import Sidebar from '../../components/widgets/sidebar/Sidebar';
import sidebar from '../../config/sidebar';
import User from '../../models/User';

const mapStateToProps = state => ({
    i18n: state.i18n,
    user: new User(state.authorization.user)
});

const mapActionsToProps = dispatch => ({
    /**
     * Return the sidebar configuration
     */
    getConfiguration() {
        return sidebar();
    }
});

export default connect(mapStateToProps, mapActionsToProps)(Sidebar);