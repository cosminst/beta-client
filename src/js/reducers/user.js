export const USER_LOGIN_ACTION = '@@authorization/user_login';
export const USER_LOGOUT_ACTION = '@@authorization/user_logout';
export const PROMPT_LOGIN = '@@authorization/prompt';
export const HIDE_PROMPT = '@@authorization/prompt_hide';

export const userLoggedIn = user => {
    return {
        type: USER_LOGIN_ACTION,
        payload: user
    };
};

export const userLoggedOut = () => {
    return {
        type: USER_LOGOUT_ACTION,
        payload: null
    };
};

export const promptLogin = message => {
    return {
        type: PROMPT_LOGIN,
        payload: message
    };
};

export const hidePrompt = () => {
    return {
        type: HIDE_PROMPT,
        payload: null
    };
};

const initialState = {
    loggedIn: false,
    user: null,
    prompt: false,
    promptMessage: null
};

const stateProcessor = {
    [USER_LOGIN_ACTION]: (state, action) => ({
        ...state,
        loggedIn: true,
        prompt: false,
        promptMessage: null,
        user: action.payload.json()
    }),
    [USER_LOGOUT_ACTION]: state => ({...state, loggedIn: false, user: null}),
    [PROMPT_LOGIN]: (state, action) => ({...state, prompt: true, promptMessage: action.payload}),
    [HIDE_PROMPT]: state => ({...state, prompt: false, promptMessage: null}),
};

export default (state = initialState, action) => {
    if (action.type in stateProcessor) {
        state = stateProcessor[action.type](state, action);
    }

    return state;
};