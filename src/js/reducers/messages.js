import Message from '../utils/Message';

export const PUSH_MESSAGE = '@@messages/push';
export const DELETE_MESSAGE = '@@messages/delete';

export const message = (message, type) => {
    return {
        type: PUSH_MESSAGE,
        payload: {message, type}
    };
};

export const deleteMessage = message => {
    return {
        type: DELETE_MESSAGE,
        payload: {message}
    }
};

const initialState = [];

const stateProcessor = {
    [PUSH_MESSAGE]: (state, action) => {
        const message = new Message(action.payload.message, action.payload.type);
        state.push(message);
        return Array.from(state);
    },
    [DELETE_MESSAGE]: (state, action) => {
        return state.slice().filter(m => m.id !== action.payload.message.id);
    }
};

export default (state = initialState, action) => {
    if (action.type in stateProcessor) {
        state = stateProcessor[action.type](state, action);
    }

    return state;
};