import React from 'react';
import hoistStatics from 'hoist-non-react-statics';

export default Component => Context => {
    const WithContext = props =>
        <Context.Consumer>
            {
                ctx =>
                    <Component {...props} context={ctx} />
            }
        </Context.Consumer>;

    const ForwardRef = React.forwardRef(
        (props, ref) => <WithContext {...props} ref={ref} />
    );

    ForwardRef.displayName = `WithContext(${Component.displayName || Component.name || 'Component'})`;
    hoistStatics(ForwardRef, Component);

    return ForwardRef;
};
