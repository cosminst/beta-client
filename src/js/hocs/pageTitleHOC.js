import React from 'react';
import i18next from 'i18next';

export default (Component, pageTitle, options = null) => {
    return props => {
        document.title = i18next.t(pageTitle, options) + ' - bookspace';
        return (
            <Component {...props}/>
        );
    };
};