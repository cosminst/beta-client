import React from 'react';
import i18next from 'i18next';

const old = i18next.t;

/**
 * Translate a key
 * @param {string} key - the translation key
 * @param {Object} options - the translation options
 * @return {string|Object}
 */
i18next.t = function (key, options = {}) {
    options = options || {};
    if (options.escape !== false) {
        return old.call(i18next, key, options);
    } else {
        return (
            <span dangerouslySetInnerHTML={{
                __html: old.call(i18next, key, {
                    ...options,
                    interpolation: {escapeValue: false}
                })
            }}/>
        );
    }
};