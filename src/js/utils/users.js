import storageWrapper from './storageWrapper';
import User from '../models/User';
import base64 from 'base-64';
import {userLoggedIn} from '../reducers/user';
import di from '../services/di';

export default () => ({
    /**
     * Initializes the current user
     * @return {Promise<User>}
     */
    initCurrentUser() {
        return new Promise(resolve => {
            let currentUser = null;
            if (this.isUserLoggedIn()) {
                const user = storageWrapper().get('user');
                currentUser = new User(user);

                const dispatch = di().getDispatcher();

                dispatch(userLoggedIn(currentUser));
            }

            resolve(currentUser);
        });
    },

    /**
     * Check if any user is logged in on the current session
     * @return {boolean}
     */
    isUserLoggedIn() {
        let user = storageWrapper().get('user');

        if (user) {
            user = new User(user);

            if (typeof user.get('token') === 'string') {
                return this.isTokenValid(user.get('token'));
            } else {
                return false;
            }
        } else {
            return false;
        }
    },

    /**
     * Checks if the user's token expired or not
     * @return {boolean}
     */
    isTokenValid(token) {
        let content = base64.decode(token.split('.')[1]);
        content = JSON.parse(content);
        return content.exp > (Date.now() / 1000);
    }
});