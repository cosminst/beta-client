export default () => ({

    /**
     * Returns a hex code for the given string
     * @param {string} string - the string to be converted to a hexcode
     * @return {string} - the hexcode
     */
    fromString(string) {
        let hash = 0;
        for (let i = 0; i < string.length; i++) {
            hash = string.charCodeAt(i) + ((hash << 5) - hash);
        }

        let color = '#';
        for (let i = 0; i < 3; i++) {
            const value = (hash >> (i * 8)) & 0xFF;
            color += ('aa' + value.toString(16)).substr(-2);
        }

        return color;
    }
});