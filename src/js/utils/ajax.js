import {Response} from "./enums";
import storageWrapper from "./storageWrapper";
import users from "./users";
import di from "../services/di";
import {userLoggedOut} from "../reducers/user";

export const GET = 'GET';
export const POST = 'POST';
export const PUT = 'PUT';
export const DELETE = 'DELETE';

export default url => ({
    /**
     * @type {{
     *     method: (string),
     *     headers: (Object),
     *     body: (Object|null)
     * }}
     */
    params: {
        method: GET,
        headers: {
            'Content-Type': 'application/json'
        },
        body: null
    },

    /**
     * Converts the given object into a valid query string and appends it to the URL
     * @param {Object} query - the object to be converted
     */
    query(query) {
        url += "?";
        Object.keys(query).forEach(key => {
            const value = query[key];
            switch (typeof value) {
                case "string":
                case "number":
                    url += `${key}=${value}&`;
                    break;
                case "object":
                    if (Array.isArray(value)) {
                        value.forEach((e, idx) => {
                            url += `${key}[${idx}]=${e}&`;
                        });
                    } else {
                        Object.keys(value).forEach(e => {
                            url += `${key}[${e}]=${value[e]}&`;
                        });
                    }
                    break;
            }
        });
        url = url.substr(0, url.length - 1);
        return this;
    },

    /**
     * Sets request method to GET
     * @param {number} expected - the expected response code
     * @return {Promise<*>}
     */
    async get(expected = Response.Ok) {
        this.params.method = GET;
        return this.send(null, expected);
    },

    /**
     * Sets request method to POST
     * @param {Object} data - the request body
     * @param {number} expected - the expected response code
     * @return {Promise<*>}
     */
    async post(data, expected = Response.Created) {
        this.params.method = POST;
        return this.send(data, expected);
    },

    /**
     * Sets request method to PUT
     * @param {Object} data - the request body
     * @param {number} expected - the expected response code
     * @return {Promise<*>}
     */
    async put(data, expected = Response.NoContent) {
        this.params.method = PUT;
        return this.send(data, expected);
    },

    /**
     * Sets request method to DELETE
     * @param {number} expected - the expected response code
     * @return {Promise<*>}
     */
    async delete(expected = Response.NoContent) {
        this.params.method = DELETE;
        return this.send(null, expected);
    },

    /**
     * Sets the authorization header on the request to the passed token
     * @return {this}
     */
    authorize() {
        const user = storageWrapper().get('user');

        if (user) {
            this.params.headers.authorization = `Bearer ${user.token}`;
        }

        return this;
    },

    /**
     * Sets a header for the request
     * @param {string} header - the header to be set
     * @param {string} value - the header's value
     * @return {this}
     */
    header(header, value) {
        this.params.headers[header] = value;
        return this;
    },

    /**
     * Sends the data to the given URL
     * @param {object|null} data - data to be sent to the API
     * @param {number} expectedCode - the expected response status code
     * @return {Promise<*>} - promise fulfilled after the request finished
     */
    async send(data = null, expectedCode) {
        return fetch(window.getAPIHost() + url, {
            method: this.params.method,
            headers: new Headers(this.params.headers),
            body: data && JSON.stringify(data)
        })
            .then(response => {
                if (response.status !== expectedCode) {
                    throw response;
                } else {
                    return response;
                }
            })
            .catch(res => {
                if (res.status === Response.Unauthorized) {
                    storageWrapper().delete('user');
                    di().getDispatcher()(userLoggedOut());
                }

                throw res;
            })
            .then(response => response.json().catch(() => null))
            .then(response => {
                if (response && response.error) {
                    throw response;
                } else {
                    return response;
                }
            });
    }
});