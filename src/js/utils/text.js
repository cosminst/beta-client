import i18next from 'i18next';

export default () => ({
    /**
     * Transforms into a boolean Yes/No value
     * @param {boolean} boolean - the value to be converted
     * @return {string}
     */
    bool(boolean) {
        return i18next.t(String(boolean));
    },

    /**
     * Converts from snake case to camelCase
     * @param {string} string - the string to be converted
     * @return {string}
     */
    toCamelCase(string) {
        const split = string.split(/[_\-]/)
            .filter(e => e.length > 0);

        return split.reduce((acc, word) => {
            return acc + this.upperFirst(word.toLowerCase());
        }, split.shift().toLowerCase());
    },

    /**
     * Transforms the string so that it has a capital first letter
     * @param {string} string - the string to be capitalized
     * @return {string}
     */
    upperFirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
});