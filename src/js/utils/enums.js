export const Color = {
    WHITE: '#fff',
    BLACK: '#000',
    PRIMARY: '#2b5787',
    THRESHOLD: parseInt('999999', 16)
};

export const Response = {
    Ok: 200,
    Created: 201,
    NoContent: 204,
    BadRequest: 400,
    Unauthorized: 401,
    AccessDenied: 403,
    NotFound: 404
};