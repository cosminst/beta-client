class Message {
    static SUCCESS = 'success';
    static ERROR = 'error';
    static INFO = 'info';

    static Instances = 0;

    id = ++Message.Instances;
    message = '';
    type = Message.SUCCESS;

    /**
     * @constructor
     * @param {string} message - the messages text
     * @param {string} type - the messages type
     */
    constructor(message, type = Message.SUCCESS) {
        this.message = message;
        this.type = type;
    }
}

export default Message;