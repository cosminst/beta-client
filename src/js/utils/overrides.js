/**
 * Checks if a number is between other 2 numbers
 * @param {number} a - smaller number
 * @param {number} b - larger number
 * @return {boolean}
 */
Number.prototype.between = function (a, b) {
    if (a > b) {
        return this.between(b, a);
    }

    return this.valueOf() >= a && this.valueOf() <= b;
};