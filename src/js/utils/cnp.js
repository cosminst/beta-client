import moment from 'moment';

export default () => ({

    /**
     * Extracts the birthdate from the CNP
     * @param {string} cnp - the CNP
     * @return {moment.Moment}
     */
    extractBirthDate(cnp) {
        const birthDate = Array.from(cnp.substr(1, 6)).reduce((date, char, idx) => {
            return date + char + (idx % 2 === 1 ? '-' : '');
        }, '');

        return moment(birthDate);
    }
});