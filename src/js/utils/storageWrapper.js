export default () => ({

    /**
     * Retrieves a key from the local storage
     * @param {string} key - the key to be retrieved
     * @return {Object|null}
     */
    get(key) {
        const value = localStorage.getItem(key);
        if (value !== null) {
            return JSON.parse(value);
        } else {
            return null;
        }
    },

    /**
     * Sets a value on the local storage
     * @param {string} key - the key which should hold the localStorage
     * @param {*} value - the value to be stored
     * @return {this}
     */
    set(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
        return this;
    },

    /**
     * Deletes a key from the local storage
     * @param {string} key - the key to be deleted
     * @return {this}
     */
    delete(key) {
        localStorage.removeItem(key);
        return this;
    }
});