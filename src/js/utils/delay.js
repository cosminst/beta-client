export default amount =>
    new Promise(resolve => setTimeout(resolve, amount));