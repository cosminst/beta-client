import React from 'react';

const OldComponent = React.Component;

class NewComponent extends OldComponent {
    /**
     * Overrides setState so it returns a promise as well
     * @param {*} state - state to be set
     * @param {function} callback - function to be executed after the set has been set
     * @return {Promise}
     * noinspection JSCheckFunctionSignatures
     */
    setState(state, callback = null) {
        const setState = new Promise(resolve => {
            super.setState(state, () => resolve(this.state));
        });

        return setState.then(state => {
            typeof callback === 'function' && callback.call(this, state);
            return state;
        });
    }
}

React.Component = NewComponent;

export default NewComponent;