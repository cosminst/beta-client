import LandingContainer from '../containers/pages/Landing/LandingContainer';
import BookListContainer from '../containers/pages/Books/BookListContainer';
import Libraries from '../components/pages/Libraries/Libraries';
import MyLibraryContainer from '../containers/pages/MyLibrary/MyLibraryContainer';

export default () => ([
    {
        key: 'landing',
        path: '/',
        component: LandingContainer,
        exact: true
    },
    {
        key: 'books',
        path: '/books/:isbn(\\d{13}|\\d{9}-[A-Za-z0-9])?',
        component: BookListContainer
    },
    {
        key: 'libraries',
        path: '/libraries',
        component: Libraries,
        flags: ['d']
    },
    {
        key: 'my-library',
        path: '/my-library(/import)?',
        component: MyLibraryContainer,
        flags: ['e']
    }
]);