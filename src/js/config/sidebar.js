import i18next from 'i18next';
import storageWrapper from '../utils/storageWrapper';

export default () => [
    {
        key: 'home',
        title: i18next.t('sidebar:home'),
        route: 'home',
        icon: 'home'
    },
    {
        key: 'books',
        title: i18next.t('sidebar:books'),
        route: 'books',
        icon: 'book'
    },
    {
        key: 'libraries',
        title: i18next.t('sidebar:libraries'),
        route: 'libraries',
        icon: 'account_balance',
        flags: ['d']
    },
    {
        key: 'my-library',
        title: i18next.t('sidebar:myLibrary'),
        route: 'my-library',
        icon: 'perm_media',
        flags: ['e']
    },
    {
        key: 'locale',
        title: i18next.t('sidebar:changeLocale'),
        icon: 'language',
        click: () => {
            const {language} = storageWrapper().get('app-settings');
            window.lang(language === 'en' ? 'ro' : 'en');
        }
    },
    {
        key: 'logout',
        title: i18next.t('sidebar:logout'),
        icon: 'exit_to_app',
        click: () => window.logout()
    }
];