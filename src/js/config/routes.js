export const routes = {
    home: '/',
    books: '/books',
    book: '/books/:isbn',
    login: '/login',
    register: '/register',
    libraries: '/libraries',
    'my-library': '/my-library',
    import: '/my-library/import'
};

export default route => {
    if (routes.hasOwnProperty(route)) {
        return routes[route];
    } else {
        throw new Error(`No such route "${route}".`);
    }
};