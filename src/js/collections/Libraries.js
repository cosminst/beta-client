import Collection from './Collection';
import Library from '../models/Library';

class Libraries extends Collection {
    static model = Library;
}

export default Libraries;