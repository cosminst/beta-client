import Collection from "./Collection";
import Book from "../models/Book";

class Books extends Collection {
    static model = Book;
}

export default Books;