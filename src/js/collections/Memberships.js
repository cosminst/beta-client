import Collection from './Collection';
import Membership from '../models/Membership';

class Memberships extends Collection {
    static model = Membership;
}

export default Memberships;