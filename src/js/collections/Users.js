import Collection from './Collection';
import User from '../models/User';

class Users extends Collection {
    static model = User;
}

export default Users;