import Collection from './Collection';
import Rental from '../models/Rental';

class Rentals extends Collection {
    static model = Rental;

    /**
     * @type {User}
     */
    rentee = null;

    /**
     * Sets the rentee used for filtering this collection
     * @param rentee
     */
    setRentee(rentee) {
        this.rentee = rentee;
        return this;
    }

    /**
     * @inheritDoc
     */
    getURL() {
        return Rental.URL
            .replace(/:rentee/, this.rentee.getIdentifier());
    }
}

export default Rentals;