import ajax from "../utils/ajax";

class Collection {

    /**
     * @private
     * @type {Model[]}
     */
    models = [];

    /**
     * @private
     * @type {Object}
     */
    metadata = {};

    /**
     * @constructor
     * @param {Model[]} models - models list
     */
    constructor(models = []) {
        this.models = models;
    }

    /**
     * Adds a model at the end of the collection
     * @param {Model} model - the model to be added
     * @return {this}
     */
    push(model) {
        this.models.push(model);
        return this;
    }

    /**
     * Checks if any of the models matches a condition
     * @param {function} callback - the condition
     * @return {boolean}
     */
    some(callback) {
        return this.models.some(callback);
    }

    /**
     * Executes a function for every element in the collection
     * @param {function} callback - the function to be executed
     * @return {this}
     */
    each(callback) {
        this.models.forEach(callback);
        return this;
    }

    /**
     * Executes a callback for every element in this collection and returns an array with the returned values
     * @param {function} callback - the callback to be executed
     */
    map(callback) {
        return this.models.map(callback);
    }

    /**
     * Returns the first model from the collection
     * @param {function} callback - callback for filter
     * @return {Model}
     */
    first(callback = null) {
        if (typeof callback === 'function') {
            return this.localFilter(callback).at(0);
        } else {
            return this.models[0];
        }
    }

    /**
     * Returns the last model from the collection
     * @param {function} callback - callback for filter
     * @return {Model}
     */
    last(callback = null) {
        if (callback) {
            return this.localFilter(callback).last();
        } else {
            return this.models[this.models.length - 1];
        }
    }

    /**
     * Retrieve the model at given index
     * @param {number} idx - the index of the model to return
     * @return {Model}
     */
    at(idx) {
        return this.models[idx] || null;
    }

    /**
     * Retrieve the model by its identifier
     * @param {string|number} id - the id of the model
     * @return {Model}
     */
    get(id) {
        return this.models.find(model => model.getIdentifier() === id);
    }

    /**
     * Filters the models and returns a collection containing the required models
     * @param {object} options - the filter data
     * @return {Promise<Collection>} - fulfilled when the collection is populated
     */
    filter(options = {}) {
        return ajax(this.getURL())
            .query(options)
            .authorize()
            .get().then(response => {
                const rawModels = response.data;
                this.models = [];
                rawModels.map(rawModel => {
                    return new this.constructor.model(rawModel);
                }).forEach(this.push.bind(this));

                return this;
            }).catch(() => this);
    }

    /**
     * Reduces the collection to a single value
     * @param {function} callback - the callback to be executed
     * @param {*} initialValue - the initial value
     */
    reduce(callback, initialValue) {
        return this.models.reduce(callback, initialValue);
    }

    /**
     * Filters this collection using a given callback
     * @param {function} callback - the callback used to check if a value should be kept or not
     * @return {Collection}
     */
    localFilter(callback) {
        const models = this.models.slice(0);

        const filteredModels = models.filter(callback);
        return new this.constructor(filteredModels);
    }

    /**
     * Counts the elements that match the callback
     * @param {function} callback - the callback
     * @return {number}
     */
    count(callback = x => x) {
        return this.models.reduce((result, m) => {
            return result + Number(callback(m));
        }, 0);
    }

    /**
     * Transforms this collection into an array of json objects
     * @return {Array} - array of json objects
     */
    json() {
        return this.models.map(model => model.json());
    }

    /**
     * Sets the metadata on this collection
     * @param {Object} metadata - the metadata to be set
     * @return {this}
     */
    setMetadata(metadata) {
        Object.assign(this.metadata, metadata);
        return this;
    }

    /**
     * Returns the URL for the ajax requests
     * @return {string}
     */
    getURL() {
        return this.constructor.model.URL;
    }

    /**
     * Retrieves the length of this collection
     * @return {number}
     */
    get length() {
        return this.models.length;
    }
}

export default Collection;