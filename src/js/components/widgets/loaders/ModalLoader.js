import React from 'react';
import i18next from 'i18next';
import {Button, Modal} from "react-bootstrap";
import Icon from "../Icon";

const ModalLoader = props => {
    const [close, toggleClose] = React.useState(false);

    React.useEffect(() => {
        setTimeout(() => {
            toggleClose(true);
        }, 5000);
    }, [1]);

    return (
        <Modal fullWidth={400} open={true}>
            <Modal.Body className={'modal-loader'}>
                <div className={'icon'}/>
                <h4>{i18next.t('misc:loader.modal.sorting')}</h4>
                {
                    close &&
                    <Button onClick={props.onClose}>
                        <Icon>close</Icon>
                    </Button>
                }
            </Modal.Body>
        </Modal>
    );
};

export default ModalLoader;