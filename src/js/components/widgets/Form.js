import React from 'react';

const Form = props => (
    <form onSubmit={e => {
        e.preventDefault();
        props.onSubmit();
    }}>
        {props.children}
    </form>
);

Form.defaultProps = {
    onSubmit: () => {}
};

export default Form;