import React from 'react';
import classnames from 'classnames';
import {Button} from 'react-bootstrap';
import color from '../../utils/color';

export default props => {
    const user = props.user;

    const className = classnames('user-picture', props.className);

    if (user.get('profilePicture') !== null) {
        return <img src={user.get('profilePicture')} alt={user.get('name')} className={className}/>
    } else {
        return <Initials user={user} className={className} onClick={props.onClick}/>
    }
};

export const Initials = props => {
    const initials = props.user.getInitials();

    return (
        <Button className={props.className} style={computeStyle(props.user)} onClick={props.onClick}>
            <span className={'initials'}>
                {initials}
            </span>
        </Button>
    );
};

Initials.defaultProps = {
    onClick: () => {}
};

const computeStyle = user => ({
    background: color().fromString(user.getInitials())
});