import React from 'react';
import classnames from 'classnames';
import {DropdownButton} from 'react-bootstrap';
import Icon from '../Icon';
import DropdownItem from './components/DropdownItem';
import {arrayOf, bool, func, oneOfType, shape, string} from 'prop-types';


class ActionButtons extends React.Component {
    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <div className={classnames('action-buttons', this.props.className)}>
                <DropdownButton title={<Icon>more_vert</Icon>} id={this.props.id}>
                    {
                        this.props.actions.map(action => (
                            <DropdownItem
                                key={action.key}
                                confirmation={action.confirmation}
                                action={action.onActionClick}
                                label={action.label}
                                div={action.div}
                            />
                        ))
                    }
                </DropdownButton>
            </div>
        );
    }
}

ActionButtons.propTypes = {
    actions: arrayOf(
        oneOfType(
            [
                shape({
                    key: string.isRequired,
                    label: string.isRequired,
                    onActionClick: func.isRequired,
                    confirmation: shape({
                        title: string.isRequired,
                        body: string.isRequired
                    })
                }),
                shape({
                    div: bool.isRequired
                })
            ]
        )
    ).isRequired
};

export default ActionButtons;
