import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import i18next from 'i18next';

export default props => (
    <Modal show={true} onHide={props.hide} className={'dialog'}>
        <Modal.Title as={'h5'}>
            {props.title}
        </Modal.Title>
        <Modal.Body>
            {props.body}
        </Modal.Body>
        <Modal.Footer>
            <Button variant={'default'} onClick={props.hide}>
                {i18next.t('false')}
            </Button>
            <Button onClick={e => {
                props.action();
                props.hide();
            }}>
                {i18next.t('true')}
            </Button>
        </Modal.Footer>
    </Modal>
);