import React from 'react';
import {Dropdown} from 'react-bootstrap';
import ActionConfirmationModal from './ActionConfirmationModal';

const DropdownItem = props => {
    const [showModal, toggleModal] = React.useState(false);

    if (props.div === true) {
        return <Dropdown.Divider/>;
    }

    return (
        <React.Fragment>
            <Dropdown.Item as={'button'} onClick={e => {
                e.preventDefault();

                if (props.confirmation) {
                    toggleModal(true);
                } else if (typeof props.action === 'function') {
                    props.action();
                }
            }}>
                {props.label}
            </Dropdown.Item>
            {
                showModal && props.confirmation &&
                <ActionConfirmationModal
                    title={props.confirmation.title}
                    body={props.confirmation.body}
                    action={props.action}
                    hide={() => toggleModal(false)}
                />
            }
        </React.Fragment>
    );
};

export default DropdownItem;