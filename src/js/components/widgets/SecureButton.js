import React from 'react';
import {Button} from 'react-bootstrap';
import di from "../../services/di";
import {connect} from "react-redux";

const mapStateToProps = state => ({
    isLoggedIn: state.authorization.loggedIn
});

const mapActionsToProps = dispatch => ({
    /**
     * Prompts the login form
     * @param {string} promptMessage - the messages to prompt in the modal
     */
    promptLogin(promptMessage) {
        di().get('authorization').promptLogin(promptMessage);
    }
});

const SecureButton = ({promptMessage, onClick, promptLogin, isLoggedIn, ...rest}) => (
    <Button
        {...rest}
        onClick={e => {
            if (isLoggedIn) {
                onClick(e);
            } else {
                promptLogin(promptMessage);
            }
        }}
    />
);

SecureButton.defaultProps = {
    onClick: () => {},
    promptMessage: ''
};

export default connect(mapStateToProps, mapActionsToProps)(SecureButton);