import React from 'react';
import classnames from 'classnames';

export default props =>
    <div className={classnames('pair', props.className)}>
        <div className={'key'}>
            {props.title}
        </div>
        <div className={'value'}>
            {props.value}
        </div>
    </div>