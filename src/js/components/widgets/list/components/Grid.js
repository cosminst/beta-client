import React from 'react';
import {ListContext} from '../../../../utils/contexts';
import {Table} from 'react-bootstrap';
import i18next from 'i18next';
import Icon from '../../Icon';

const cell = (key, value) => (
    <td key={key} className={`cell cell-${key}`}>
        {value}
    </td>
);

export default props => {
    const ctx = React.useContext(ListContext);

    return (
        <Table striped hover>
            <thead>
            <tr>
                {
                    props.columns.map(c => {
                        if (typeof c === 'function') {
                            c = c().key;
                        }

                        return (
                            <th key={c} className={`header header-${c}`}>
                                {i18next.t(`list:${ctx.scope}.header.${c}`)}
                            </th>
                        );
                    })
                }
            </tr>
            </thead>
            <tbody>
            {
                ctx.collection.map((m, idx) => {
                    return (
                        <tr
                            key={m.getIdentifier() || idx}
                            id={`row-${m.getIdentifier() || idx}`}
                            onClick={() => ctx.toggleForm(true, m)}
                        >
                            {
                                props.columns.map(c => {
                                    if (typeof c === 'function') {
                                        c = c(m);
                                    }

                                    return typeof c === 'object' ? cell(c.key, c.value) : cell(c, m.get(c));
                                })
                            }
                        </tr>
                    );
                })
            }
            <tr className={'add-button'}>
                <td
                    colSpan={props.columns.length}
                    onClick={() => ctx.toggleForm(true, new ctx.collection.constructor.model())}
                >
                    <Icon>add</Icon>
                    <span>
                        {i18next.t(`list:${ctx.scope}.add`)}
                    </span>
                </td>
            </tr>
            </tbody>
        </Table>
    );
};