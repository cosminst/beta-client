import React from 'react';
import * as PropTypes from 'prop-types';
import Collection from '../../../collections/Collection';
import {ListContext} from '../../../utils/contexts';
import Grid from './components/Grid';

const DEFAULT_LIMIT = 20;

class List extends React.Component {
    static propTypes = {
        collection: PropTypes.func.isRequired,
    };

    static Grid = Grid;

    state = {
        collection: new Collection(),
        setCollection: coll => this.setState({collection: coll}),
        toggleForm: this.toggleForm.bind(this),
        scope: this.props.scope || 'list',
        focused: new this.props.collection.model(),
        showForm: false,
        save: this.save.bind(this)
    };

    /**
     * Called after the component rendered;
     */
    componentDidMount() {
        this.props.fetchList(this.props.filters).then(collection => {
            this.setState({collection}, this.props.onCollectionUpdate);
        });
    }

    /**
     * Toggles the form on or off
     * @param {boolean} on - show or do not show the form
     * @param {Model} model - the model to use for the form
     */
    toggleForm(on, model = null) {
        this.setState({
            showForm: on,
            focused: model || new this.props.collection.model()
        });
    }

    /**
     * Saves the currently focused model
     * @return {Promise}
     */
    save() {
        return this.state.focused
            .save()
            .then(model => {
                if (!this.state.collection.some(model.equals)) {
                    this.state.collection.push(model);
                }
            })
            .then(() => {
                this.state.toggleForm(false);
            });
    }

    /**
     * Render function.
     * @return {Object}
     */
    render() {
        return (
            <div className={'list'}>
                <ListContext.Provider value={this.state}>
                    {this.props.children}
                </ListContext.Provider>
            </div>
        );
    }
}

List.defaultProps = {
    onCollectionUpdate: () => {
    }
};

export default List;