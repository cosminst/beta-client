import React from 'react';
import {OverlayTrigger, Popover} from 'react-bootstrap';

export default props => {
    const popover = (
        <Popover id={props.id}>
            <Popover.Title>{props.title}</Popover.Title>
        </Popover>
    );

    return (
        <OverlayTrigger overlay={popover} placement={'right'}>
            {props.children}
        </OverlayTrigger>
    );
};