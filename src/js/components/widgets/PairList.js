import React from 'react';
import classnames from 'classnames';

const PairList = props => (
    <div className={classnames('pair-list', props.className, props.style)}>
        {props.children}
    </div>
);

PairList.Chained = props => (
    <PairList style={'chained'} {...props} />
);

PairList.Listed = props => (
    <PairList style={'listed'} {...props}/>
);

export default PairList;