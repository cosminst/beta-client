import React from 'react';
import {Link as RouterLink} from 'react-router-dom';
import PropTypes from 'prop-types';
import di from '../../services/di';

class Link extends React.Component {
    static propTypes = {
        to: PropTypes.string.isRequired,
        params: PropTypes.object
    };

    /**
     * Object state
     * @type {Object}
     */
    state = {};

    /**
     * Executed right before render function
     * @param {Object} props - component props
     * @param {Object} state - current object state
     * @return {Object} - the modified state object
     */
    static getDerivedStateFromProps(props, state) {
        const computedUrl = di().get('navigation')
            .getComputedURL(props.to, props.params);

        if (computedUrl !== state.to) {
            return {to: computedUrl};
        }

        return null;
    }

    /**
     * Render function
     * @return {*}
     */
    render() {
        return (
            <RouterLink to={this.state.to} className={this.props.className}>
                {this.props.children}
            </RouterLink>
        );
    }
}

export default Link;