import React from 'react';

export default props =>
    <img src={`https://via.placeholder.com/${props.width}x${props.height}`} {...props}/>