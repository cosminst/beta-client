import React from 'react';
import classnames from 'classnames';
import RadioContext from './RadioContext';

const RadioButton = props => {
    const ctx = React.useContext(RadioContext);

    return (
        <span className={classnames('radio-button', props.className)}>
            <span
                className={classnames('radio', {checked: ctx.selected === props.value})}
                onClick={() => ctx.onChange(props.value)}
            />
            <label>
                {props.label}
                <input
                    type={'radio'}
                    value={props.value}
                    style={{display: 'none'}}
                    checked={ctx.selected === props.value}
                    onChange={() => ctx.onChange(props.value)}
                />
            </label>
        </span>
    );
};

export default RadioButton;