import React from 'react';
import RadioButton from './RadioButton';
import RadioContext from './RadioContext';

class Radio extends React.Component {
    static Option = RadioButton;

    /**
     * @type {{selected: null}}
     */
    state = {
        selected: this.props.defaultValue,
        onChange: this.handleValueChange.bind(this),
    };

    /**
     * Updates the selected value
     * @param {*} value - the new selected value
     */
    handleValueChange(value) {
        this.setState({selected: value}, () => {
            this.props.onChange(value);
        });
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <div className={'radio-group'}>
                <RadioContext.Provider value={this.state}>
                    {
                        React.Children.map(this.props.children, child => {
                            return React.cloneElement(child, {
                                name: this.props.name,
                            });
                        })
                    }
                </RadioContext.Provider>
            </div>
        );
    }
}

Radio.defaultProps = {
    onChange: () => {}
};

export default Radio;