import 'date-fns';
import React from 'react';
import DateFNSUtils from '@date-io/date-fns';
import {DatePicker as MuiDatePicker, MuiPickersUtilsProvider} from 'material-ui-pickers';
import moment from 'moment';

class DatePicker extends React.Component {

    /**
     * @constructor
     * @param {Object} props - the component's props
     */
    constructor(props) {
        super(props);

        this.state = {
            value: props.defaultValue
        };
    }

    /**
     * Return the field value
     * @return {*}
     */
    getFieldValue() {
        return this.state.value;
    }

    /**
     * Handle selecting another date
     * @param {Object} value - the selected date
     */
    handleChange(value) {
        this.setState({value})
            .then(state => {
                this.props.onChange(moment(state.value));
            });
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <MuiPickersUtilsProvider utils={DateFNSUtils}>
                <MuiDatePicker
                    value={this.state.value}
                    onChange={e => this.handleChange(e)}
                    label={this.props.label}
                    autoOk
                />
            </MuiPickersUtilsProvider>
        );
    }
}

DatePicker.defaultProps = {
    onChange: () => {}
};

export default DatePicker;