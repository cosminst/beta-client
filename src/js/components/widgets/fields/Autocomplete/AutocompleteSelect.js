import React from 'react';
import Select from 'react-select';
import PropTypes from 'prop-types';
import classnames from 'classnames';

class AutocompleteSelect extends React.Component {
    scheduledRequest = null;
    state = {
        loading: false,
        options: [],
        value: null
    };

    /**
     * @constructor
     * @param {Object} props - component's props
     */
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleInputChange = this.handleInputChange.bind(this);
    }


    /**
     * Called after the component is done rendering
     */
    componentDidMount() {
        return this.fetchOptions(this.props.filters);
    }

    /**
     * Fetches the options
     * @param {Object} filters - filters to use for the fetch
     * @return {Promise}
     */
    fetchOptions(filters) {
        this.setState({loading: true}).then(() => {
            return this.props.dataSource.filter(filters);
        }).then(collection => {
            const options = collection.map(model => ({
                value: model.get(this.props.valueKey),
                label: model.get(this.props.labelKey)
            }));

            return this.setState({options, loading: false});
        });
    }

    /**
     * Handles the autocomplete value change
     * @param {*} value - the new value that was set
     */
    handleChange(value) {
        this.setState({value}).then(() => {
            const model = this.props.dataSource.get(value.value);
            this.props.onChange(model);
        });
    }

    /**
     * Handles the user typing in the autocomplete input
     * @param {string} value - the written value
     */
    handleInputChange(value) {
        if (value !== this.lastRequestValue) {
            if (this.scheduledRequest) {
                clearTimeout(this.scheduledRequest);
            }

            this.scheduledRequest = setTimeout(() => {
                let filters = Object.assign({}, this.props.filters);
                if (value.length > 0) {
                    filters[this.props.labelKey] = value;
                }

                this.lastRequestValue = value;
                return this.fetchOptions(filters);
            }, 300);
        }
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        const labelClasses = classnames(['control-label', {
            'required': this.props.required
        }]);

        return (
            <div className={'form-group'}>
                <div className={'material'}>
                    <label className={labelClasses}>
                        {this.props.label}
                    </label>
                    <Select
                        defaultValue={this.props.defaultValue}
                        isDisabled={this.props.disabled}
                        className={this.props.className}
                        isLoading={this.state.loading}
                        isClearable={this.props.clearable}
                        isSearchable={true}
                        onInputChange={this.handleInputChange}
                        options={this.state.options}
                        onChange={this.handleChange}
                        value={this.state.value}
                        placeholder={this.props.placeholder || ''}
                    />
                    <i className={'bar'}/>
                </div>
            </div>
        );
    }
}

AutocompleteSelect.propTypes = {
    className: PropTypes.string,
    defaultValue: PropTypes.any,
    disabled: PropTypes.bool,
    loading: PropTypes.bool,
    clearable: PropTypes.bool,
    dataSource: PropTypes.shape({
        filter: PropTypes.func
    }).isRequired,
    valueKey: PropTypes.string.isRequired,
    labelKey: PropTypes.string.isRequired,
    filters: PropTypes.object,
    placeholder: PropTypes.string,
    label: PropTypes.string,
    classNamePrefix: PropTypes.string,
    required: PropTypes.bool,
    onChange: PropTypes.func,
};

AutocompleteSelect.defaultProps = {
    disabled: false,
    loading: false,
    clearable: true,
    dataSource: {filter: () => Promise.reject()},
    valueKey: '_id',
    labelKey: 'name',
    filters: {},
    required: false,
    onChange: value => {
    }
};

export default AutocompleteSelect;