import React from 'react';

export default props => {
    return (
        <div className={'autocomplete-option'}>
            <h5>{props.data.label}</h5>
            {
                props.data.description && <p>{props.data.description}</p>
            }
        </div>
    );
}