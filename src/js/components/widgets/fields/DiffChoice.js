import React from 'react';
import RadioButton from './RadioButton';
import * as PropTypes from 'prop-types';
import Collection from '../../../collections/Collection';
import i18next from 'i18next';
import text from '../../../utils/text';

class DiffChoice extends React.Component {
    static propTypes = {
        titleKey: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        models: PropTypes.instanceOf(Collection).isRequired,
        onChange: PropTypes.func
    };

    /**
     * @constructor
     * @param {Object} props - the component props
     */
    constructor(props) {
        super(props);

        this.state = {
            value: props.models.first() || null
        };
    }

    /**
     * Handles the change of the radio
     * @param {string} choice - the changed value
     */
    handleChange(choice) {
        const value = this.props.models.get(choice);
        this.setState({value})
            .then(state => this.props.onChange(state.value));
    }

    /**
     * @return {Model}
     */
    getFieldValue() {
        return this.state.value;
    }

    /**
     * Formats the given field value
     * @param {*} value
     */
    format(value) {
        switch (typeof value) {
            case 'boolean':
                return text().bool(value);
            case 'number':
                return value > 100 ? '∞' : value;
            case 'string':
                return i18next.t(`fields:${this.props.name}.` + text().toCamelCase(value));
        }
    }

    /**
     * Gets the title string
     * @param {Model} model - the model
     * @return {string}
     */
    getTitle(model) {
        return i18next.t(`fields:${this.props.name}.` + text().toCamelCase(model.get(this.props.titleKey)));
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <div className={'diff-choice'}>
                {
                    this.props.models.map(model => {
                        return (
                            <div key={model.getIdentifier()} className={'choice'}>
                                <div className={'choice-control'}>
                                    <RadioButton
                                        name={this.props.name}
                                        value={model.getIdentifier()}
                                        radioClass={'radio'}
                                        checked={model.sameAs(this.state.value)}
                                        onChange={this.handleChange.bind(this)}
                                    >
                                        {this.getTitle(model)}
                                    </RadioButton>
                                </div>
                                <div className={'choice-details'}>
                                    {
                                        model.iterate((value, key) => {
                                            return (
                                                <div key={key} className={'diff'}>
                                                    <span className={'diff-key'}>
                                                        {i18next.t('fields:' + this.props.name + '.keys.' + key)}
                                                    </span>
                                                    <span className={'diff-value'}>
                                                        {this.format(value)}
                                                    </span>
                                                </div>
                                            );
                                        })
                                    }
                                </div>
                            </div>
                        );
                    })
                }
            </div>
        );
    }
}

DiffChoice.defaultProps = {
    onChange: () => {
    }
};

export default DiffChoice;