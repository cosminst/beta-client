import React from 'react';
import * as PropTypes from 'prop-types';

class RadioButton extends React.Component {
    static propTypes = {
        id: PropTypes.string,
        name: PropTypes.string.isRequired,
        className: PropTypes.string,
        value: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]).isRequired,
        labelClass: PropTypes.string,
        radioClass: PropTypes.string
    };

    /**
     * Handle the radio button's change of state
     * @param {Object} e - the checked state
     */
    handleChange(e) {
        this.props.onChange(this.props.value);
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <label htmlFor={this.props.id} className={this.props.labelClass}>
                <input
                    type={'radio'}
                    id={this.props.id}
                    name={this.props.name}
                    className={this.props.radioClass}
                    onChange={this.handleChange.bind(this)}
                    value={this.props.value}
                    checked={this.props.checked}
                />
                {this.props.children}
            </label>
        );
    }
}

RadioButton.defaultProps = {
    onChange: () => {}
};

export default RadioButton;