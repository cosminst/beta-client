import React from 'react';
import i18next from 'i18next';
import classnames from 'classnames';
import {Form} from 'react-bootstrap';
import Icon from '../Icon';

class TextField extends React.Component {
    static Instances = 0;
    state = {};

    static getDerivedStateFromProps(props, state) {
        if (state.value !== props.value) {
            return {
                value: props.value
            };
        } else {
            return state;
        }
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        const {error, className, ...rest} = this.props;
        const cls = classnames(className, 'field');

        this.id = this.id || rest.id || `textField#${++TextField.Instances}`;

        return (
            <Form.Group className={cls}>
                <Form.Label title={rest.label} htmlFor={this.id}>
                    {rest.label}
                </Form.Label>
                <Form.Control
                    type={rest.type}
                    id={this.id}
                    placeholder={rest.placeholder}
                    className={classnames({'has-error': !!error})}
                    value={this.state.value || ""}
                    onChange={e => this.props.onChange(e.target.value)}
                    disabled={rest.disabled}
                />
                {
                    error &&
                    <div className={'field-error'}>
                        {i18next.t(error.message, error.options)}
                    </div>
                }
            </Form.Group>
        );
    }
}

TextField.defaultProps = {
    onChange: () => {
    }
};

export default TextField;

