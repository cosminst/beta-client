import React from 'react';
import ReactDOM from 'react-dom';

export default props =>
    ReactDOM.createPortal(
        <div className={'loader'}>
        </div>,
        document.getElementById('loaderContainer')
    )