import React from 'react';
import TextField from "./fields/TextField";
import i18next from 'i18next';
import User from "../../models/User";
import di from "../../services/di";
import {userLoggedIn} from "../../reducers/user";
import {Modal, Button} from "react-bootstrap";

export function login(email, password) {
    const user = new User({
        email,
        password
    });

    user.login().then(() => {
        user.unset('password');
        di().getDispatcher()(userLoggedIn(user));
    });
}

export default props => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');

    return (
        <Modal show={props.show} onHide={props.hide} className={'prompt-modal'}>
            <Modal.Title className={'login-title'}>
                {i18next.t('general:greeting', {context: 'anon'})}
            </Modal.Title>
            <Modal.Body>
                <div className={'login-form'}>
                    <TextField
                        name={'email'}
                        label={i18next.t('fields:loginPrompt.email')}
                        defaultValue={email}
                        onChange={value => setEmail(value)}
                    />
                    <TextField
                        name={'password'}
                        type={'password'}
                        label={i18next.t('fields:loginPrompt.password')}
                        defaultValue={password}
                        onChange={value => setPassword(value)}
                    />
                    <p className={'calibri'}>* {props.message}</p>
                    <Button className={'btn-primary login-btn'} onClick={() => login(email, password)}>
                        {i18next.t('fields:loginPrompt:login')}
                    </Button>
                </div>
            </Modal.Body>
        </Modal>
    );
};
