import React from 'react';
import Language from './Language';

export default props => {
    return (
        <div className={'language-selector'}>
            <Language code={'en'}/>
            <Language code={'ro'}/>
        </div>
    );
};