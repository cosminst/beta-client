import React from 'react';
import ReactDOM from 'react-dom';
import {deleteMessage} from '../../../reducers/messages';
import {connect} from 'react-redux';
import Icon from '../Icon';

const MessageContainer = {
    State: state => ({
        messages: state.messages
    }),

    Actions: dispatch => ({
        /**
         * Deletes a message
         * @param {Message} message
         * @return {*}
         */
        deleteMessage(message) {
            dispatch(deleteMessage(message));
        }
    })
};

const Messages = props => ReactDOM.createPortal(
    props.messages.map(m => <Message key={m.id} message={m} hide={props.deleteMessage}/>)
, document.getElementById('messages'));

const Message = ({message: m, hide}) => {
    React.useEffect(() => {
        setTimeout(() => {
            hide(m);
        }, 3000);
    }, [1]);

    return (
        <div className={'message message-' + m.type}>
            <span className={'close-btn'} onClick={() => hide(m)}>
                <Icon>close</Icon>
            </span>
            {m.message}
        </div>
    );
};

export default connect(MessageContainer.State, MessageContainer.Actions)(Messages);