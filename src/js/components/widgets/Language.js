import React from 'react';
import Flag from 'react-flagkit';
import {Button} from 'react-bootstrap';
import di from '../../services/di';
import {connect} from 'react-redux';

const countryCodes = {
    en: 'US',
    ro: 'RO'
};

const mapActionsToProps = dispatch => ({
    /**
     * Changes the app language
     * @param {string} code - the language code
     * @return {Promise}
     */
    changeLanguage(code) {
        return di().get('localization')
            .changeLanguage(code);
    }
});

export default connect(null, mapActionsToProps)(props =>
    <Button className={'flag'} onClick={() => props.changeLanguage(props.code)}>
        <Flag country={countryCodes[props.code]} className={'flag-image'}/>
    </Button>
);