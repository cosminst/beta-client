import React from 'react';
import classnames from 'classnames';

export default props =>
    <i className={classnames('material-icons', props.className)}>
        {props.children}
    </i>