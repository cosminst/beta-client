import React from 'react';
import Link from '../Link';
import classnames from 'classnames';
import Icon from '../Icon';
import MenuItemPopover from '../popovers/MenuItemPopover';

class Sidebar extends React.Component {

    /**
     * Checks the flag required
     * @param {Object} entry - the sidebar entry
     * @return {boolean}
     */
    isAllowed(entry) {
        return this.props.user.hasAccessFlags(entry.flags);
    }

    /**
     * Render function
     * @return {*}
     */
    render() {
        return (
            <div className={classnames('sidebar', 'closed')}>
                <ul className={'menu-list'}>
                    {
                        this.props.getConfiguration()
                            .filter(this.isAllowed.bind(this))
                            .map(entry =>
                                <MenuItemPopover key={entry.key} id={entry.key} title={entry.title} description={''}>
                                    <li key={entry.key} className={'menu-item calibri'}>
                                        {
                                            !entry.click ? (
                                                <Link to={entry.route} params={entry.parameters}>
                                                    <Icon>{entry.icon}</Icon>
                                                </Link>
                                            ) : (
                                                <a onClick={entry.click}>
                                                    <Icon>{entry.icon}</Icon>
                                                </a>
                                            )
                                        }
                                    </li>
                                </MenuItemPopover>
                            )
                    }
                </ul>
            </div>
        );
    }
}

export default Sidebar;