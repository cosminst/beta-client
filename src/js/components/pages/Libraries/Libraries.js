import React from 'react';
import List from '../../../containers/widgets/ListContainer';
import Libraries from '../../../collections/Libraries';
import LibraryForm from './LibraryForm';
import pageTitleHOC from '../../../hocs/pageTitleHOC';

const columns = [
    'name',
    'name_canonical',
    model => ({
        key: 'librarian',
        value: model && model.get('librarian') && model.get('librarian').get('name')
    }),
    'bookCount'
];

const LibrariesPage = props => {
    return (
        <div id={'libraries-page'}>
            <List collection={Libraries} scope={'libraries'}>
                <List.Grid columns={columns}/>
                <LibraryForm/>
            </List>
        </div>
    );
};

export default pageTitleHOC(LibrariesPage, 'misc:libraries.title');