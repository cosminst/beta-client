import React from 'react';
import Form from '../../widgets/Form';
import TextField from '../../widgets/fields/TextField';
import {Button, Modal} from 'react-bootstrap';
import i18next from 'i18next';
import {ListContext} from '../../../utils/contexts';
import AutocompleteSelect from '../../widgets/fields/Autocomplete/AutocompleteSelect';
import Users from '../../../collections/Users';

const submit = ctx => data => {
    ctx.focused.set('name', data.name)
        .set('name_canonical', data.nameCanonical)
        .set('librarian', data.librarian);

    ctx.save();
};

export default props => {
    const ctx = React.useContext(ListContext);

    const [name, setName] = React.useState('');
    const [nameCanonical, setNameCanonical] = React.useState('');
    const [librarian, setLibrarian] = React.useState(null);

    React.useEffect(() => {
        setName(ctx.focused.get('name'));
        setNameCanonical(ctx.focused.get('name_canonical'));
    }, [ctx.focused]);

    return (
        <Modal key={ctx.focused.getIdentifier()} className={'form-modal'} show={ctx.showForm} onHide={() => ctx.toggleForm(false)}>
            <Modal.Header>
                <Modal.Title>
                    {i18next.t('fields:libraries.formTitle', {
                        context: ctx.focused.getIdentifier() ? 'edit' : 'add',
                        name: ctx.focused.get('name')
                    })}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form onSubmit={() => submit(ctx)({name, nameCanonical})}>
                    <TextField
                        name={'name'}
                        label={i18next.t('fields:libraries.name')}
                        value={name}
                        onChange={setName}
                        disabled={!!ctx.focused.getIdentifier()}
                    />
                    <TextField
                        name={'name_canonical'}
                        label={i18next.t('fields:libraries.name_canonical')}
                        value={nameCanonical}
                        onChange={setNameCanonical}
                        disabled={!!ctx.focused.getIdentifier()}
                    />
                    {
                        ctx.focused.getIdentifier() &&
                        <AutocompleteSelect
                            dataSource={new Users()}
                            label={i18next.t('fields:libraries.users')}
                            value={librarian ? librarian.getIdentifier() : null}
                            onChange={setLibrarian}
                        />
                    }
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button variant={'default'} onClick={() => ctx.toggleForm(false)}>
                    {i18next.t('close')}
                </Button>
                <Button onClick={() => submit(ctx)({name, nameCanonical, librarian})}>
                    {i18next.t('fields:libraries.submit', {
                        context: ctx.focused.getIdentifier() && 'edit'
                    })}
                </Button>
            </Modal.Footer>
        </Modal>
    );
};