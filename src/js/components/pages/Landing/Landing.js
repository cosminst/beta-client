import React from 'react';
import DiffChoice from '../../widgets/fields/DiffChoice';
import Memberships from '../../../collections/Memberships';
import Loader from '../../widgets/Loader';
import i18next from 'i18next';
import RegisterForm from '../../forms/registration/RegisterForm';
import {Button} from 'react-bootstrap';
import pageTitleHOC from '../../../hocs/pageTitleHOC';
import Radio from '../../widgets/fields/Radio/Radio';
import MyRentals from './components/MyRentals';

class Landing extends React.Component {
    /** @type {RegisterForm} */
    form = null;

    /** @type {DiffChoice} */
    membershipField = null;

    state = {
        loading: true,
        membershipTypes: new Memberships()
    };

    /**
     * Executed after the page loaded
     */
    componentDidMount() {
        this.props.getMembershipTypes()
            .then(membershipTypes => this.setState({membershipTypes, loading: false}));
    }

    /**
     * Submits the registration form
     */
    submit() {
        const value = this.form.getValue();
        const membershipType = this.membershipField.getFieldValue().getIdentifier();

        const membershipValue = Object.assign(value, {membershipType});

        this.props.submitRegistration(membershipValue);
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <div className={'landing'}>
                <MyRentals/>
            </div>
        );
    }
}

export default pageTitleHOC(Landing, 'Landing page');