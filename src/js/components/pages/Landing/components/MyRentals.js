import React from 'react';
import i18next from 'i18next';
import User from '../../../../models/User';
import Rentals from '../../../../collections/Rentals';
import usePromise from '../../../../hooks/usePromise';
import {connect} from 'react-redux';
import CarouselLoader from '../../../widgets/loaders/CarouselLoader';
import MyRentalCard from './MyRentalCard';
import Rental from '../../../../models/Rental';
import {message} from '../../../../reducers/messages';
import Message from '../../../../utils/Message';

const MyRentalsContainer = {
    State: state => ({
        user: new User(state.authorization.user)
    }),

    Actions: dispatch => ({

        /**
         * Fetches the current employee's rentals
         * @return {Promise<Rentals>}
         */
        getMyRentals() {
            return new Rentals()
                .setRentee(this.user)
                .filter();
        },

        /**
         * Cancels a given rental
         * @param {Rental} rental - the rental to cancel
         * @return {Promise}
         */
        cancel(rental) {
            rental.set('status', Rental.STATUS_CANCELLED);

            const text = 'books:rental.action.cancel.message';
            return rental.save()
                .then(() => dispatch(message(i18next.t(text, {context: 'success'}))))
                .catch(() => dispatch(message(i18next.t(text, {context: 'error'}), Message.ERROR)));
        }
    })
};

const STATUS_ORDER = [
    Rental.STATUS_DUE,
    Rental.STATUS_DELIVERED,
    Rental.STATUS_TO_BE_DELIVERED,
    Rental.STATUS_NEW,
    Rental.STATUS_CANCELLED
];

const MyRentals = props => {
    const {loading, result} = usePromise(props.getMyRentals());

    return (
        <div className={'my-rentals'}>
            <h5 className={'calibri'}>{i18next.t('books:myRentals')}</h5>
            <div className={'rentals-list'}>
                {
                    loading ? (
                        <CarouselLoader/>
                    ) : (
                        STATUS_ORDER.map(section => result.count(r => r.get('status') === section) > 0 && (
                            <div className={'rental-section rental-section-' + section} key={section}>
                                <h6 className={'calibri'}>{i18next.t('books:rental.status.' + section)}</h6>
                                <div className={'section-cards'}>
                                    {
                                        result.localFilter(r => r.get('status') === section)
                                            .map(r => (
                                                <MyRentalCard
                                                    key={r.getIdentifier()}
                                                    rental={r}
                                                    cancel={props.cancel}
                                                    showActions={[Rental.STATUS_NEW, Rental.STATUS_TO_BE_DELIVERED, Rental.STATUS_DELIVERED].includes(r.get('status'))}
                                                />
                                            ))
                                    }
                                </div>
                            </div>
                        ))
                    )
                }
            </div>
        </div>
    );
};

export default connect(MyRentalsContainer.State, MyRentalsContainer.Actions)(MyRentals);

