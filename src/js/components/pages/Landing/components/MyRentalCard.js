import React from 'react';
import {Card} from 'react-bootstrap';
import ActionButtons from '../../../widgets/actions/ActionButtons';
import i18next from 'i18next';

export default ({rental, ...props}) => (
    <Card className={'my-rental-card'}>
        <Card.Body>
            <h5 className={'calibri bold'}>
                {rental.get('book').get('title')}
                <br/>
                <small>{rental.get('book').get('author')}</small>
            </h5>
            <small className={'calibri rental-card-lib'}>
                {rental.get('library').get('name')}
            </small>
            {
                props.showActions &&
                <ActionButtons actions={[
                    {
                        key: 'cancel',
                        label: i18next.t('books:rental.action.cancel.label'),
                        onActionClick: () => props.cancel(rental),
                        confirmation: {
                            title: i18next.t('books:rental.action.cancel.title'),
                            body: i18next.t('books:rental.action.cancel.body')
                        }
                    }
                ]}/>
            }
        </Card.Body>
    </Card>
);