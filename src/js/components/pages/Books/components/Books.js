import React from 'react';
import {ListContext} from '../../../../utils/contexts';
import Link from '../../../widgets/Link';
import i18next from 'i18next';
import Pair from '../../../widgets/Pair';
import {Card} from "react-bootstrap";
import PairList from "../../../widgets/PairList";

export default props => {
    const ctx = React.useContext(ListContext);

    return ctx.collection.map(book => {
        return (
            <Card className={'book-card'} key={book.getIdentifier()}>
                <Card.Title>
                    <Link to={'book'} params={{isbn: book.get('isbn')}}>
                        {book.get('title')}
                    </Link>
                </Card.Title>
                <Card.Body>
                    <PairList.Listed>
                        <Pair title={i18next.t('books:author')} value={book.get('author')}/>
                        <Pair title={i18next.t('books:year')} value={book.get('year')}/>
                    </PairList.Listed>
                </Card.Body>
                <Card.Footer>
                    <small>
                        {book.get('libraries').first().get('name')}
                    </small>
                </Card.Footer>
            </Card>
        );
    });
};
