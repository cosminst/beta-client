import React from 'react';
import {ListContext} from '../../../../utils/contexts';
import i18next from 'i18next';
import Books from '../../../../collections/Books';
import TextField from "../../../widgets/fields/TextField";

let scheduledRequest = null;
let lastRequestedTitle = '';

const handleSearchBarChange = (ctx, setValue) => value => {
    const title = value.trim();

    setValue(value);

    if (title !== lastRequestedTitle) {
        if (scheduledRequest) {
            clearTimeout(scheduledRequest);
        }

        scheduledRequest = setTimeout(() => {
            const filter = title ? {title} : {};
            new Books().filter(filter).then(collection => {
                lastRequestedTitle = title;
                return ctx.setCollection(collection);
            });
        }, 300);
    }
};

export default props => {
    const ctx = React.useContext(ListContext);
    const [value, setValue] = React.useState('');

    return (
        <div className={'search-bar'}>
            <TextField
                label={i18next.t('books:findABook')}
                className={'find'}
                value={value}
                onChange={handleSearchBarChange(ctx, setValue)}
            />
        </div>
    );
};