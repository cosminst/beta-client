import React from 'react';
import List from '../../../containers/widgets/ListContainer';
import Books from '../../../collections/Books';
import BooksCards from './components/Books';
import SearchBar from './components/SearchBar';
import pageTitleHOC from '../../../hocs/pageTitleHOC';
import BookModal from './BookModal';

class BookPage extends React.Component {
    state = {modal: false};

    /**
     * Sets whether the modal should be visible or not
     * @static
     * @param {Object} props - the component props
     * @param {Object} state - the component state
     */
    static getDerivedStateFromProps(props, state) {
        return {
            modal: props.isBookPage()
        };
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        return (
            <div className={'book-page'}>
                <List collection={Books}>
                    <SearchBar/>
                    <div className={'list-wrapper'}>
                        <div className={'list-items'}>
                            <BooksCards/>
                        </div>
                    </div>
                    {
                        this.state.modal &&
                        <BookModal
                            onClose={this.props.hideModal}
                            book={this.props.getUrlBook()}
                        />
                    }
                </List>
            </div>
        );
    }
}

export default pageTitleHOC(BookPage, 'books:pageTitle');