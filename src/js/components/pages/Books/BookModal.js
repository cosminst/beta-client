import React from 'react';
import {Modal, Button} from 'react-bootstrap';
import i18next from 'i18next';
import PlaceholderImage from '../../widgets/PlaceholderImage';
import PairList from '../../widgets/PairList';
import Pair from '../../widgets/Pair';
import SecureButton from '../../widgets/SecureButton';
import ModalLoader from '../../widgets/loaders/ModalLoader';
import Rental from '../../../models/Rental';
import {connect} from 'react-redux';

const BookModalContainer = {
    Actions: () => ({
        /**
         * Rents a book
         * @param {Book} book - the book to rent
         * @return {Promise}
         */
        rent(book) {
            return new Rental()
                .set('book', book)
                .save();
        }
    })
};

class BookModal extends React.Component {
    state = {book: null, loading: false, hasRental: false};

    /**
     * Fetches the book
     */
    componentDidMount() {
        this.props.book
            .fetch()
            .then(book => {
                this.setState({book, hasRental: book.get('hasRental')});
            });
    }

    /**
     * Rents the book
     */
    rentBook() {
        this.setState({loading: true}).then(() => {
            return this.props.rent(this.state.book);
        }).then(() => {
            this.state.book.add('stock', -1);
        }).finally(() => {
            return this.setState({loading: false, hasRental: true});
        });
    }

    /**
     * Returns the button label
     * @return {string}
     */
    getButtonText() {
        let label = 'books:outOfStock';

        if (this.state.book.get('stock') > 0) {
            label = 'books:rentThisBook';

            if (this.state.hasRental) {
                label = 'books:alreadyRented';
            }
        }

        return i18next.t(label);
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        const {book, loading} = this.state;

        if (loading) {
            return <ModalLoader onClose={() => this.setState({loading: false})}/>;
        }

        return (
            <Modal className={'book-modal'} onHide={this.props.onClose} show>
                {
                    book &&
                    <React.Fragment>
                        <div className={'book-picture'}>
                            <PlaceholderImage width={256} height={388} alt={'Book picture'}/>
                        </div>
                        <div className={'book-details'}>
                            <Modal.Header>
                                <Modal.Title>
                                    {book.get('title')}
                                </Modal.Title>
                            </Modal.Header>
                            <Modal.Body>
                                <div className={'book-content calibri'}>
                                    <PairList.Chained>
                                        <Pair title={i18next.t('books:author')} value={book.get('author')}/>
                                        <Pair title={i18next.t('books:year')} value={book.get('year')}/>
                                        <Pair title={i18next.t('books:stock')} value={book.get('stock')}/>
                                        <Pair title={i18next.t('books:synopsis')} value={book.get('synopsis')}/>
                                    </PairList.Chained>
                                </div>
                            </Modal.Body>
                            <Modal.Footer>
                                <small>
                                    {i18next.t('books:librariesAvailable', {
                                        count: book.get('libraries').length,
                                        library: book.get('libraries').at(0).get('name'),
                                        escape: false
                                    })}
                                </small>
                                <Button onClick={this.props.onClose}>
                                    {i18next.t('close')}
                                </Button>
                                <SecureButton
                                    className={'btn btn-primary'}
                                    promptMessage={i18next.t('misc:loginPrompt.rent')}
                                    onClick={this.rentBook.bind(this)}
                                    disabled={this.state.book.get('stock') <= 0 || this.state.hasRental}
                                >
                                    {this.getButtonText()}
                                </SecureButton>
                            </Modal.Footer>
                        </div>
                    </React.Fragment>
                }
            </Modal>
        );
    }
}

export default connect(null, BookModalContainer.Actions)(BookModal);
