import React from 'react';
import {Button, Modal} from 'react-bootstrap';
import RegisterForm from '../../forms/registration/RegisterForm';

export default props => {
    return (
        <Modal show={props.show} onHide={props.hide}>
            <Modal.Header>
                <Modal.Title>
                    Register
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <RegisterForm/>
            </Modal.Body>
            <Modal.Footer>
                <Button variant={'default'}>Cancel</Button>
                <Button>Submit</Button>
            </Modal.Footer>
        </Modal>
    );
};