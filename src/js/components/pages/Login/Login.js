import React from 'react';
import {Button, Card, Col, Row} from 'react-bootstrap';
import i18next from 'i18next';
import TextField from '../../widgets/fields/TextField';
import User from '../../../models/User';
import {connect} from 'react-redux';
import di from '../../../services/di';
import {userLoggedIn} from '../../../reducers/user';
import Form from '../../widgets/Form';
import Link from '../../widgets/Link';
import {routes} from '../../../config/routes';
import Register from './Register';
import pageTitleHOC from '../../../hocs/pageTitleHOC';

const LoginContainer = {
    Actions: dispatch => ({

        /**
         * Goes back to the login page
         */
        back() {
            return di().get('navigation').navigate('login');
        },

        /**
         * Authenticates the user
         * @param {string} email - the email
         * @param {string} password - the password
         * @return {Promise<User>}
         */
        authenticate(email, password) {
            const user = new User({
                email,
                password
            });

            return user.login()
                .then(user => dispatch(userLoggedIn(user.unset('password'))))
                .then(() => di().get('navigation').navigate('home'));
        }
    })
};

const Login = props => {
    const [email, setEmail] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [register, toggleRegister] = React.useState(false);

    React.useEffect(() => {
        toggleRegister(props.location.pathname === routes.register);
    });

    return (
        <React.Fragment>
            <div className={'login-page'}>
                <Row>
                    <Col md={5}>
                        <Form onSubmit={() => props.authenticate(email, password)}>
                            <Card className={'login-form'}>
                                <Card.Title>{i18next.t('misc:login.title')}</Card.Title>
                                <Card.Body>
                                    <TextField
                                        name={'email'}
                                        label={i18next.t('fields:loginPrompt.email')}
                                        onChange={setEmail}
                                        value={email}
                                    />
                                    <TextField
                                        type={'password'}
                                        name={'password'}
                                        label={i18next.t('fields:loginPrompt.password')}
                                        onChange={setPassword}
                                        value={password}
                                    />
                                </Card.Body>
                                <Card.Footer>
                                    <Link to={'register'}>{i18next.t('misc:login.noAccount')}</Link>
                                    <Button type={'submit'} onClick={() => props.authenticate(email, password)}
                                            className={'float-right'}>
                                        {i18next.t('fields:loginPrompt.login')}
                                    </Button>
                                </Card.Footer>
                            </Card>
                        </Form>
                    </Col>
                </Row>
            </div>
            <Register
                show={register}
                hide={() => {
                    toggleRegister(false);
                    props.back();
                }}
            />
        </React.Fragment>
    );
};

export default pageTitleHOC(connect(null, LoginContainer.Actions)(Login), 'misc:login.title');
