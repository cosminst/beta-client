import React from 'react';
import usePromise from '../../../hooks/usePromise';
import Library from '../../../models/Library';
import i18next from 'i18next';
import List from '../../../containers/widgets/ListContainer';
import Books from '../../../collections/Books';
import BookCard from './components/BookCard';
import {Button} from 'react-bootstrap';
import {routes} from '../../../config/routes';
import ImportModal from './components/ImportModal';
import di from '../../../services/di';
import pageTitleHOC from '../../../hocs/pageTitleHOC';

const MyLibrary = props => {
    const [importModal, toggleImportModal] = React.useState(props.location.pathname === routes.import);
    const {result: library, loading} = usePromise(new Library().getMyLibrary());

    React.useEffect(() => {
        toggleImportModal(props.location.pathname === routes.import);
    });

    if (loading) {
        return null;
    }

    return (
        <div className={'my-library'}>
            <h2>{library.get('name')}</h2>
            <br/>
            <div className={'my-library-books'}>
                <h4>
                    {i18next.t('misc:libraries.booksInMyLibrary')}
                    <Button variant={'default'} onClick={() => di().get('navigation').navigate('import')}>
                        {i18next.t('misc:libraries.import')}
                    </Button>
                </h4>
                <List collection={Books} filters={{library: library.getIdentifier()}}>
                    <BookCard/>
                </List>
            </div>
            <ImportModal
                show={importModal}
                hide={() => {
                    toggleImportModal(false);
                    di().get('navigation').goBack();
                }}
                onChange={data => {
                    props.import(data).finally(() => {
                        toggleImportModal(false);
                        props.goBack();
                    });
                }}
            />
        </div>
    );
};

export default pageTitleHOC(MyLibrary, 'misc:libraries.myLibrary');