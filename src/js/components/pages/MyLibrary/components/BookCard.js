import React from 'react';
import {ListContext} from '../../../../utils/contexts';
import {Card} from 'react-bootstrap';

export default props => {
    const ctx = React.useContext(ListContext);

    return (
        <div className={'list-wrapper'}>
            <div className={'list-items'}>
                {ctx.collection.map((m, idx) => (
                    <Card key={m.getIdentifier() + '-' + idx}>
                        <Card.Body>
                            <strong>{m.get('title')}<br/></strong>
                            <small>{m.get('author')}</small>
                            <small className={'isbn'}>{m.get('isbn')}</small>
                        </Card.Body>
                    </Card>
                ))}
            </div>
        </div>
    );
}