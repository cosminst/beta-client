import React from 'react';
import {Button, Form, Modal} from 'react-bootstrap';
import i18next from 'i18next';

let reader = null;

export default props => {
    const [file, setFile] = React.useState(null);

    return (
        <Modal show={props.show} onHide={props.hide}>
            <Modal.Header>
                <Modal.Title>
                    {i18next.t('misc:libraries.importModal.title')}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form.Label>
                    {i18next.t('misc:libraries.importModal.label')}
                    <Form.Control type={'file'} style={{marginTop: 10}} onChange={evt => {
                        reader = new FileReader();
                        reader.onload = e => {
                            props.onChange(e.currentTarget.result)
                        };

                        setFile(evt.target.files[0]);
                    }}/>
                </Form.Label>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => {
                    reader.readAsDataURL(file);
                }}>
                    {i18next.t('ok')}
                </Button>
                <Button variant={'default'} onClick={() => props.hide()}>
                    {i18next.t('close')}
                </Button>
            </Modal.Footer>
        </Modal>
    );
};