import React from 'react';
import {Route, Switch} from 'react-router';
import SidebarContainer from '../../containers/widgets/SidebarContainer';
import LoginPrompt from "../widgets/LoginPrompt";

export default props => {
    React.useEffect(() => {
        if (props.user.empty()) {
            props.logout();
        }
    });

    return (
        <div className={'content-wrapper'} id={'gateway'}>
            <SidebarContainer/>
            <div className={'content'}>
                <Switch>
                    {
                        props.getConfiguration()
                            .filter(route => props.user.hasAccessFlags(route.flags))
                            .map(route => <Route {...route} />)
                    }
                </Switch>
            </div>
            <LoginPrompt
                show={props.showPrompt}
                message={props.promptMessage}
                hide={props.hidePrompt}
            />
        </div>
    );
};
