import React from 'react';
import TextField from '../../widgets/fields/TextField';
import i18next from 'i18next';
import RegisterContext from './RegisterContext';
import {Col, Row} from 'react-bootstrap';

export default () => {
    const ctx = React.useContext(RegisterContext);

    return (
        <React.Fragment>
            <Row>
                <Col sm={5}>
                    <TextField
                        name={'firstName'}
                        label={i18next.t('fields:registration.firstName')}
                        ref={ref => ctx.fields.firstName = ref}
                        onChange={value => ctx.handleFieldChange('firstName', value)}
                        defaultValue={ctx.value.firstName}
                    />
                </Col>
                <Col sm={5}>
                    <TextField
                        name={'lastName'}
                        label={i18next.t('fields:registration.lastName')}
                        ref={ref => ctx.fields.lastName = ref}
                        onChange={value => ctx.handleFieldChange('lastName', value)}
                        defaultValue={ctx.value.firstName}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={5}>
                    <TextField
                        name={'nationalIdentityNumber'}
                        label={i18next.t('fields:registration.nationalIdentityNumber')}
                        ref={ref => ctx.fields.nationalIdentityNumber = ref}
                        onChange={value => ctx.handleFieldChange('nationalIdentityNumber', value)}
                        defaultValue={ctx.value.firstName}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={3}>
                    <TextField
                        name={'city'}
                        label={i18next.t('fields:registration.city')}
                        ref={ref => ctx.fields.city = ref}
                        onChange={value => ctx.handleFieldChange('city', value)}
                        defaultValue={ctx.value.city}
                    />
                </Col>
                <Col sm={3}>
                    <TextField
                        name={'county'}
                        label={i18next.t('fields:registration.county')}
                        ref={ref => ctx.fields.county = ref}
                        onChange={value => ctx.handleFieldChange('county', value)}
                        defaultValue={ctx.value.county}
                    />
                </Col>
                <Col sm={3}>
                    <TextField
                        name={'street'}
                        label={i18next.t('fields:registration.street')}
                        ref={ref => ctx.fields.street = ref}
                        onChange={value => ctx.handleFieldChange('street', value)}
                        defaultValue={ctx.value.street}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={3}>
                    <TextField
                        name={'streetNumber'}
                        label={i18next.t('fields:registration.streetNumber')}
                        ref={ref => ctx.fields.streetNumber = ref}
                        onChange={value => ctx.handleFieldChange('streetNumber', value)}
                        defaultValue={ctx.value.streetNumber}
                    />
                </Col>
                <Col sm={3}>
                    <TextField
                        name={'details'}
                        label={i18next.t('fields:registration.details')}
                        ref={ref => ctx.fields.details = ref}
                        onChange={value => ctx.handleFieldChange('details', value)}
                        defaultValue={ctx.value.details}
                    />
                </Col>
            </Row>
        </React.Fragment>
    );
}