import React from 'react';
import RegisterContext from './RegisterContext';
import TextField from '../../widgets/fields/TextField';
import i18next from 'i18next';
import {Col, Row} from 'react-bootstrap';

export default props => {
    const ctx = React.useContext(RegisterContext);

    return (
        <React.Fragment>
            <Row>
                <Col sm={7}>
                    <TextField
                        name={'email'}
                        label={i18next.t('fields:registration.email')}
                        ref={ref => ctx.fields.email = ref}
                        onChange={value => ctx.handleFieldChange('email', value)}
                        defaultValue={ctx.value.email}
                    />
                </Col>
            </Row>
            <Row>
                <Col sm={7}>
                    <TextField
                        name={'password'}
                        type={'password'}
                        label={i18next.t('fields:registration.password')}
                        ref={ref => ctx.fields.password = ref}
                        onChange={value => ctx.handleFieldChange('password', value)}
                        defaultValue={ctx.value.password}
                    />
                </Col>
            </Row>
        </React.Fragment>
    );
}