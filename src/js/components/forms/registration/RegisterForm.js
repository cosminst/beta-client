import React from 'react';
import i18next from 'i18next';
import RegisterContext from './RegisterContext';
import PersonalInfo from './PersonalInfo';
import Credentials from './Credentials';
import {Button} from 'react-bootstrap';
import cnp from '../../../utils/cnp';
import Icon from '../../widgets/Icon';

class RegisterForm extends React.Component {
    static STEPS = [PersonalInfo, Credentials];
    state = {step: 0};
    fields = {};
    value = {
        firstName: '',
        lastName: '',
        nationalIdentityNumber: '',
        birthDate: '',
        email: '',
        password: '',
        city: '',
        county: '',
        street: '',
        streetNumber: '',
        details: ''
    };

    /**
     * Handles the change of any field
     * @param {string} field - the changed field name
     * @param {*} value - the new value of the field
     */
    handleFieldChange(field, value) {
        this.value[field] = value;

        if (field === 'nationalIdentityNumber' && value) {
            const date = cnp().extractBirthDate(value);
            this.value.birthDate = date.format('YYYY-MM-DD');
        }
    }


    /**
     * Get the form value
     * @return {{
     *      password: string,
     *      person: {
     *          firstName: string,
     *          lastName: string,
     *          address: {
     *              city: string,
     *              streetNumber: string,
     *              street: string,
     *              county: string,
     *              details: string
     *          },
     *          nationalIdentityNumber: string,
     *          birthDate: string
     *      },
     *      email: string
     *  }}
     */
    getValue() {
        return {
            email: this.value.email,
            password: this.value.password,
            person: {
                firstName: this.value.firstName,
                lastName: this.value.lastName,
                nationalIdentityNumber: this.value.nationalIdentityNumber,
                birthDate: this.value.birthDate,
                address: {
                    city: this.value.city,
                    county: this.value.county,
                    street: this.value.street,
                    streetNumber: this.value.streetNumber,
                    details: this.value.details
                }
            }
        }
    }

    /**
     * Returns the registration context
     * @return {Object}
     */
    getContext() {
        return {
            value: this.value,
            fields: this.fields,
            handleFieldChange: (field, value) => this.handleFieldChange(field, value)
        };
    }

    /**
     * Navigates the stepper in a certain direction
     * @param {number} direction - the direction of the navigation
     */
    navigate(direction) {
        direction = direction / Math.abs(direction);

        this.setState({
            step: this.state.step + direction
        });
    }

    /**
     * Render function.
     * @return {*}
     */
    render() {
        const CurrentSlide = RegisterForm.STEPS[this.state.step];

        return (
            <RegisterContext.Provider value={this.getContext()}>
                <form className={'registration'} id={'registerForm'} noValidate>
                    <h2>{i18next.t('fields:registration.title')}</h2>
                    <CurrentSlide/>
                </form>
                <div className={'stepper-buttons'}>
                    {
                        this.state.step > 0 &&
                        <Button onClick={() => this.navigate(-1)}>
                            <Icon>fast_rewind</Icon>
                            {i18next.t('back')}
                        </Button>
                    }
                    {
                        this.state.step < RegisterForm.STEPS.length - 1 &&
                        <Button onClick={() => this.navigate(1)}>
                            {i18next.t('next')}
                            <Icon>fast_forward</Icon>
                        </Button>
                    }
                </div>
            </RegisterContext.Provider>
        );
    }
}

export default RegisterForm;