import '@babel/polyfill/noConflict';
import React from 'react';
import ReactDOM from 'react-dom';
import i18next from 'i18next';
import './utils/ComponentOverride';
import './utils/i18n';
import './utils/overrides';
import {Provider} from 'react-redux';
import {createStore, applyMiddleware, combineReducers, compose} from 'redux';
import {ConnectedRouter, routerMiddleware, connectRouter} from 'connected-react-router';
import {i18nReducer} from 'react-redux-i18n';
import thunk from 'redux-thunk';
import {createBrowserHistory} from 'history';
import GatewayContainer from './containers/pages/GatewayContainer';
import di from './services/di';
import storageWrapper from './utils/storageWrapper';
import users from './utils/users';
import user, {userLoggedOut} from './reducers/user';
import {Route, Switch} from 'react-router';
import Login from './components/pages/Login/Login';
import messages from './reducers/messages';
import Messages from './components/widgets/messages/Messages';

const history = createBrowserHistory();

window.isDevelopmentEnvironment = () => __isDevelopmentEnvironment__;
window.getAPIHost = () => apiHost;

let enhancers = null;
if (__isDevelopmentEnvironment__) {
    enhancers = compose(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history)),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
    );
} else {
    enhancers = compose(
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
    );
}

const store = createStore(
    combineReducers({
        i18n: i18nReducer,
        router: connectRouter(history),
        authorization: user,
        messages: messages,
    }),
    enhancers
);

window.logout = () => store.dispatch(userLoggedOut());

const userSettings = storageWrapper().get('app-settings') || {};

di().setDispatcher(store.dispatch);

const usersHelper = users();
usersHelper.initCurrentUser().then(currentUser => {
    di().get('localization')
        .changeLanguage(userSettings.language || 'en')
        .then(() => {
            window.lang = lang => di().get('localization').changeLanguage(lang);
            ReactDOM.render(
                <Provider store={store}>
                    <ConnectedRouter history={history}>
                        <Switch>
                            <Route
                                path={'/(login|register)'}
                                component={Login}
                            />
                            <GatewayContainer user={currentUser}/>
                        </Switch>
                        <Messages/>
                    </ConnectedRouter>
                </Provider>,
                document.getElementById('app-root')
            );
        });
});
