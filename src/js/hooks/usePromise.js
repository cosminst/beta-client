import React from 'react';

/**
 * Manages internal loading state and waits for the passed promise to be resolved
 * @param {Promise} promise - the promise argument
 * @return {{loading: boolean, result: Object, error: Object}}
 */
function usePromise(promise) {
    const [state, setState] = React.useState({
        loading: true,
        result: null,
        error: null
    });

    let result = null,
        error = null;

    if (state.loading) {
        promise
            .then(r => result = r)
            .catch(err => error = err)
            .finally(() => setState({
                loading: false,
                result: result || null,
                error: error || null
            }));
    }

    return state;
}

export default usePromise;