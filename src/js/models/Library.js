import Model from './Model';
import User from './User';
import ajax from '../utils/ajax';

class Library extends Model {
    static URL = '/libraries';
    static MY_LIBRARY_URL = '/my-library';

    getMyLibrary() {
        return ajax(Library.MY_LIBRARY_URL)
            .authorize()
            .get()
            .then(response => {
                this.assign(this.parse(response));
                return this;
            })
            .catch(() => null);
    }

    parse(data) {
        if (data.librarian) {
            data.librarian = new User(data.librarian);
        }

        return data;
    }

    getSaveData() {
        if (this.get('librarian')) {
            return {
                name: this.get('name'),
                name_canonical: this.get('name_canonical'),
                librarian: this.get('librarian').getIdentifier()
            };
        } else {
            return this.json();
        }
    }
}

export default Library;