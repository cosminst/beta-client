import Model from './Model';

class Membership extends Model {
    static URL = '/memberships';
    static HIDDEN_KEYS = ['_id', 'name', 'type'];

    static FREEMIUM = 1;
    static MEMBER = 2;
    static PREMIUM = 3;

    identifier = 'type';
}

export default Membership;