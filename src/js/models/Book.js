import Model from "./Model";
import Libraries from '../collections/Libraries';
import Library from './Library';

class Book extends Model {
    static URL = '/books';
    identifier = 'isbn';

    /**
     * Rents this book
     * @return {Promise}
     */
    rent() {
        return this.save();
    }

    /**
     * @inheritDoc
     */
    parse(data) {
        const parsedData = Object.assign({}, data);
        parsedData.libraries = new Libraries(data.libraries.map(library => new Library(library)));

        return parsedData;
    }
}

export default Book;