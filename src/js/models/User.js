import Model from './Model';
import Membership from './Membership';
import ajax from "../utils/ajax";
import storageWrapper from "../utils/storageWrapper";
import {Response} from "../utils/enums";

class User extends Model {
    static URL = '/users';
    static AUTH_URL = '/auth';

    static ADMINISTRATOR = 'abdefghi';
    static LIBRARIAN = 'abefi';
    static DELIVERY = 'abch';

    /**
     * @constructor
     * @param {Object} data - the model data
     */
    constructor(data = {}) {
        super(data);
        this.proxy('name', () => this.get('person').firstName + ' ' + this.get('person').lastName);
    }

    /**
     * Get this user name's initials
     * @return {string}
     */
    getInitials() {
        return this.get('firstName').charAt(0) + this.get('lastName').charAt(0);
    }

    /**
     * Logs the user in
     * @return {Promise<User>}
     */
    async login() {
        return ajax(User.AUTH_URL)
            .post({
                email: this.get('email'),
                password: this.get('password')
            }, Response.Ok).then(response => {
                this.assign(response);
                storageWrapper()
                    .set('user', this.json());
                return this;
            });
    }

    /**
     * Checks if the user has all the access flags passed
     * @param {string[]} flags - the flags to be checked for
     * @return {boolean}
     */
    hasAccessFlags(flags = []) {
        return flags.reduce((result, orFlags) => {
            return result && Array.from(orFlags).reduce((result, flag) => {
                return result || Array.from(this.get('accessFlags') || '').includes(flag);
            }, false);
        }, true);
    }

    /**
     * Checks if the user is at least a member of the platform
     * @return {boolean}
     */
    isMember() {
        return this.get('membershipType') >= Membership.MEMBER;
    }
}

export default User;