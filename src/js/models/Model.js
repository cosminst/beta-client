import ajax from '../utils/ajax';

class Model {
    static Instances = 1;
    /**
     * @type {string}
     */
    cid = 'm' + this.constructor.Instances++;

    /**
     * @private
     * @type {object|null}
     */
    data = {};

    /**
     * @protected
     * @type {Object}
     */
    proxies = {};

    /**
     * @protected
     * @type {string}
     */
    identifier = '_id';

    /**
     * @constructor
     * @param {Object} data - the model data
     */
    constructor(data = null) {
        this.assign(data ? this.parse(data) : {});
        this.equals = this.equals.bind(this);
    }

    /**
     * Gets the identifier value
     * @return {string|number}
     */
    getIdentifier() {
        return this.get(this.identifier);
    }

    /**
     * Sets the identifier value
     * @param {string|number} value - the value to be set
     * @return {this}
     */
    setIdentifier(value) {
        return this.set(this.identifier, value);
    }

    /**
     * Iterates through each key
     * @param {function} callback - the function to be executed for each key
     * @return {Array}
     */
    iterate(callback) {
        return Object.keys(this.data).map(key => {
            const value = this.get(key);
            if (this.constructor.HIDDEN_KEYS && !this.constructor.HIDDEN_KEYS.includes(key)) {
                return callback(value, key);
            } else {
                return null;
            }
        }).filter(e => e); // eliminate nulls
    }

    /**
     * Increments a property
     * @param {string} key - the key to be incremented
     * @param {number} n - the number to be added
     * @return {this}
     */
    add(key, n) {
        this.data[key] += n;
        return this;
    }

    /**
     * Retrieves the value from a given key
     * @param {string} key - the key to retrieve the value for
     * @return {*|null}
     */
    get(key) {
        if (this.data.hasOwnProperty(key)) {
            return this.data[key];
        } else if (this.proxies.hasOwnProperty(key)) {
            return this.proxies[key].get();
        } else {
            return null;
        }
    }

    /**
     * Saves this model
     * @return {Promise}
     */
    save() {
        let request = ajax(this.getURL())
            .authorize();

        if (this.getIdentifier()) {
            request = request.put(this.getSaveData());
        } else {
            request = request.post(this.getSaveData());
        }

        return request.then(response => {
            response && this.assign(this.parse(response.data));
            return this;
        });
    }

    /**
     * Checks if a model equals another
     * @param {Model} model - the other model
     * @return {boolean}
     */
    equals(model) {
        return !!model && this.getIdentifier() === model.getIdentifier();
    }

    /**
     * Fetches the model and updates it
     * @return {Promise<this>}
     */
    fetch() {
        return ajax(this.getURL())
            .authorize()
            .get()
            .then(response => {
                this.assign(this.parse(response));
                return this;
            });
    }

    /**
     * Sets a key to a given value
     * @param {string} key - the key to be set
     * @param {*} value - the value assigned to the key
     * @return {this}
     */
    set(key, value) {
        if (this.proxies.hasOwnProperty(key)) {
            this.proxies[key].set(value);
        } else {
            this.data[key] = value;
        }

        return this;
    }

    /**
     * Removes a key
     * @param {string} key - the key to be removed
     * @return {this}
     */
    unset(key) {
        delete this.data[key];
        return this;
    }

    /**
     * Assigns an object to this model
     * @param {Object} object - object to be assigned to this model's data
     */
    assign(object) {
        Object.keys(object).forEach(key => {
            const value = object[key];
            this.set(key, value);
        });

        return this;
    }

    /**
     * Parses model data before it can be set to this model
     * @param {Object} data - the model data to be parsed
     * @return {Object}
     */
    parse(data) {
        return data;
    }

    /**
     * Checks if two models refer to the same object
     * @param {Model} model - the model to be checked
     * @return {boolean}
     */
    sameAs(model) {
        return !!model && this.getIdentifier() === model.getIdentifier()
            && this.constructor === model.constructor;
    }

    /**
     * Transforms this model back into a json object
     * @return {{}}
     */
    json() {
        return Object.keys(this.data).reduce((result, key) => {
            const value = this.data[key];

            if (value && typeof value.json === 'function') {
                result[key] = value.json();
            } else if (value) {
                result[key] = value;
            }

            return result;
        }, {});
    }

    /**
     * Creates a new proxy given the key and its getter
     * @param {string} key - the proxy key
     * @param {function} get - the function called to retrieve the proxy
     * @return {this}
     */
    proxy(key, get) {
        this.proxies[key] = {get};
        return this;
    }

    /**
     * Checks if this model is empty or not
     * @return {boolean}
     */
    empty() {
        return Object.keys(this.data).length === 0;
    }

    /**
     * Gets the request URL
     * @return {string}
     */
    getURL() {
        if (this.getIdentifier()) {
            return this.constructor.URL + '/' + this.getIdentifier();
        } else {
            return this.constructor.URL;
        }
    }

    /**
     * Gets the json data that should be sent to the server
     * @return {Object}
     */
    getSaveData() {
        return this.json();
    }

    /**
     * Returns the model type as a string
     * @return {string}
     */
    getModelType() {
        return this.constructor.name;
    }
}

export default Model;