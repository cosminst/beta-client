import Model from './Model';
import Book from './Book';
import Library from './Library';
import User from './User';

class Rental extends Model {
    static URL = '/rentals';

    static STATUS_NEW = 'new';
    static STATUS_TO_BE_DELIVERED = 'to_be_delivered';
    static STATUS_DELIVERED = 'delivered';
    static STATUS_DUE = 'due';
    static STATUS_CANCELLED = 'cancelled';
    static STATUS_CLOSED = 'closed';

    /**
     * @inheritDoc
     */
    parse(data) {
        const parsedData = Object.assign({}, data);

        parsedData.book = new Book(data.book);
        parsedData.library = new Library(data.library);
        parsedData.rentee = new User(data.rentee);

        return parsedData;
    }

    /**
     * @inheritDoc
     */
    getSaveData() {
        return {
            book: this.get('book').getIdentifier(),
            status: this.get('status')
        };
    }
}

export default Rental;