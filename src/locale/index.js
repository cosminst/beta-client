export default () => [
    'general',
    'sidebar',
    'books',
    'fields',
    'misc',
    'list'
];